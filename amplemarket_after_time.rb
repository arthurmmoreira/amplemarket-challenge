# THIS CODE I FINISHED AFTER THE TIME IS COMPLETE

queue = [{}]

begin
  puts queue
  input = gets
  command, name, value = input.split(' ')
  case command
  when 'SET'
    queue.last.store(name, value)
  when 'GET'
    puts (queue.select { |q| q.key?(name) }.last || {}).fetch(name, nil) || 'NULL'
  when 'UNSET'
    queue.last.store(name, nil)
  when 'NUMEQUALTO'
    first = queue.first
    puts queue.drop(1).reduce(first) { |acc, q| acc.merge(q) }.select { |k, v| v == name }.count
  when 'BEGIN'
    queue << {}
  when 'ROLLBACK'
    if queue.count > 1
      queue.pop
    else
      puts 'NO TRANSACTION'
    end
  when 'COMMIT'
    queue = [queue.reduce(queue.shift) { |acc, q| acc.merge(q) }]
  end
end while command != 'END'
