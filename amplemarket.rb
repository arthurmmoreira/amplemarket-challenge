# THIS CODE I FINISHED WITH JOAO BATALHA

queue = [{}]

begin
  puts queue
  input = gets
  command, name, value = input.split(' ')
  case command
  when 'SET'
    queue.last.store(name, value)
  when 'GET'
    puts queue.last.fetch(name, 'NULL')
  when 'UNSET'
    queue.last.delete(name)
  when 'NUMEQUALTO'
    puts queue.last.select { |k, v| v == name }.count
  when 'BEGIN'
    queue << {}
  when 'ROLLBACK'
    if queue.count > 1
      queue.pop
    else
      puts 'NO TRANSACTION'
    end
  when 'COMMIT'
    queue = [queue.reduce(queue.shift) { |acc, q| acc.merge(q) }]
  end
end while command != 'END'
